'use strict'
const { DataTypes } = require('sequelize');
const sequelize = require('../config/sequelize');


const SampleNew = sequelize.define('SampleNew', {
    // Model attributes are defined here
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    //   get(){
    //       return this.getDataValue('name');
    //   },
    //   set(value){
    //     this.setDataValue('name',value);
    //   }
    },    
  }, {
    // Other model options go here
    timestamps: true,
    paranoid: true,
    tableName : 'samplenew'
  });
  
module.exports = SampleNew;