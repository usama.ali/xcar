'use strict'
const { Sequelize } = require('sequelize');
const config = require('config');
const sequelize = new Sequelize(config.get('db'));

const p=sequelize.authenticate();
p.then( () => {
    console.log('Connection has been established successfully.');
})
.catch((error) => {
    console.error('Unable to connect to the database:', error);
    process.exit(-1);
})
 
module.exports = sequelize;