'use strict'
const JwtStrategy = require('passport-jwt').Strategy
const ExtractJwt = require('passport-jwt').ExtractJwt;
// require('dotenv').config();
// const User = require('./../model/user');

// At a minimum, you must pass the `jwtFromRequest` and `secretOrKey` properties
const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: 'secret',
//   algorithms: ['HMAC']
};

// app.js will pass the global passport object here, and this function will configure it
module.exports = (passport) => {
  
    // The JWT payload is passed into the verify callback
    passport.use(new JwtStrategy(options,async function(jwt_payload, done) {
        try{
            console.log("In passport Strategy");
            // const user = await User.findByPk(jwt_payload.user.id);
            // if(!user || (user.status !== 3 && user.status !==6) ) return done(null,false);
            return done(null,true);            
        }catch(error){
            console.log('error: ', error);
            return done(error,false);
        }
    }));
}