const jsonwebtoken = require('jsonwebtoken');

module.exports = (req, res, next) =>{
    try{
        const token = jsonwebtoken.sign({user:{
            id:1,
            name:"xyz"
        }},"secret",{
            expiresIn:"7d"
        });

        res.status(200).send({
            status:200,
            message:"Sample is working",
            token
        })
    }catch(error){
        console.log('error: ', error);
        res.status(406).send({
            status:406,
            message:error.message
        })
    }
};