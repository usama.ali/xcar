const express = require('express');
const router = express.Router();
const passport = require('passport');
const ping = require('./api/ping/ping');
const get = require('./api/crud/get');
const sample = require('./api/sample/sample');
const sampleAuth = require('./api/sample/sampleauth');
const accessControl = require('./../middleware/access');

require('../config/passport')(passport);
router.use(passport.initialize());
/* GET home page. */
router.get('/', function(req, res, next) {

  res.status(200).send("XCar App")

});

router.get('/ping',ping);
router.get('/get',get);
router.get('/sample',sample);
router.get('/sampleauth/:id',passport.authenticate('jwt', { session: false }),accessControl,sampleAuth);

module.exports = router;
